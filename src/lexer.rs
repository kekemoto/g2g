use regex::Regex;
use std::collections::HashSet;
use std::collections::VecDeque;
use std::fs::File;
use std::io::BufRead;
use std::io::BufReader;
use std::io::Lines;

lazy_static! {
    static ref DELIMITER: HashSet<char> = {
        ['(', ')', '?', '*', '+', '|', '\n', '\r', '%']
            .into_iter()
            .cloned()
            .collect()
    };
    static ref REMOVE_DELIMITER: HashSet<char> = { [' '].into_iter().cloned().collect() };
}

pub fn lex(path: &str) -> VecDeque<String> {
    let mut lexer = Lexer::new();
    for line in file_lines(path) {
        let mut text = line.unwrap();
        text.push('\n');
        lexer.input(text);
        lexer.run();
    }
    lexer.tokens
}

fn file_lines(path: &str) -> Lines<BufReader<File>> {
    let f = File::open(path).unwrap();
    let reader = BufReader::new(f);
    reader.lines()
}

#[derive(Debug)]
struct Lexer {
    text: VecDeque<char>,
    token: VecDeque<char>,
    tokens: VecDeque<String>,
}

impl Lexer {
    fn new() -> Self {
        Lexer {
            text: VecDeque::new(),
            token: VecDeque::new(),
            tokens: VecDeque::new(),
        }
    }

    fn input(&mut self, text: String) {
        self.text = text.chars().collect();
    }

    fn run(&mut self) {
        self.parse_text();
        self.cut();
    }

    fn parse_text(&mut self) {
        match self.pop() {
            None => return,
            Some(x) => {
                if '\\' == x {
                    self.forward_escape(x);
                    self.parse_text();
                } else if '$' == x {
                    self.push(x);
                    self.parse_doller();
                } else if '\'' == x {
                    self.forward_delimiter(x);
                    self.parse_not('\'');
                } else if '/' == x {
                    self.forward_delimiter(x);
                    self.parse_not('/');
                } else if REMOVE_DELIMITER.contains(&x) {
                    self.cut();
                    self.parse_text();
                } else if DELIMITER.contains(&x) {
                    self.forward_delimiter(x);
                    self.parse_text();
                } else {
                    self.push(x);
                    self.parse_text();
                }
            }
        }
    }

    fn parse_not(&mut self, end: char) {
        self.pop().map(|x| {
            if '\\' == x {
                self.forward_escape(x);
                self.parse_not(end);
            } else if end == x {
                self.forward_delimiter(x);
                self.parse_text();
            }
        });
    }

    fn parse_doller(&mut self) {
        self.pop().map(|x| {
            if Regex::new(r"\d+|$|\(|\)|/|\?|\*|\+")
                .unwrap()
                .is_match(&x.to_string())
            {
                self.push(x);
                self.parse_doller();
            } else {
                self.cut();
                self.push(x);
                self.parse_text();
            }
        });
    }

    fn forward_escape(&mut self, delimiter: char) {
        self.push(delimiter);
        self.pop().map(|x| {
            self.push(x);
        });
    }

    fn forward_delimiter(&mut self, delimiter: char) {
        self.cut();
        self.push(delimiter);
        self.cut();
    }

    fn pop(&mut self) -> Option<char> {
        self.text.pop_front()
    }

    fn push(&mut self, input: char) {
        self.token.push_back(input)
    }

    fn cut(&mut self) {
        if self.token.is_empty() {
            return;
        }
        self.tokens.push_back(self.token.iter().collect::<String>());
        self.token = VecDeque::new();
    }
}
