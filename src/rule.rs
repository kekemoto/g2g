use regex::Regex;

type Token = Vec<char>;

struct Lex {
    token: Token,
    tokens: Vec<Token>,
}

struct RuleParser {
    code: String,
    rules: Vec<Rule>,
    lex: Lex,
}

impl RuleParser {
    fn new(code: String, rules: Vec<Rule>, lex: Lex) -> Self {
        Self {
            code: code,
            rules: rules,
            lex: lex,
        }
    }

    fn run(&mut self) -> Option<String> {
        for rule in self.rules.iter() {
            match rule.apply(&mut self.lex) {
                None => None as Option<String>,
                x => return x,
            };
        }
        None as Option<String>
    }

    fn tokenize(&mut self) {}
}

trait RuleTrait {
    fn apply(&self, lex: &mut Lex) -> Option<String>;
}

struct Rule {
    name: String,
    elements: Vec<Element>,
}

impl RuleTrait for Rule {
    fn apply(&self, lex: &mut Lex) -> Option<String> {
        None
    }
}

struct Element {
    formula: Formula,
    quantifier: Quantifier,
}

enum Formula {
    string(String),
    regex(Regex),
    rule(Rule),
    group(Group),
    or(Or),
}

struct Group {
    formulas: Vec<Element>,
}

struct Or {
    rules: Vec<Rule>,
}

struct Quantifier {
    typ: QuantifierType,
    num: u32,
    from: u32,
    until: i32,
}

enum QuantifierType {
    One,
    Question,
    Asterisk,
    Plus,
    Number,
    Range,
}

// pub struct TokenRegex {
//     text: String,
// }
//
// impl AST<Regex> for TokenRegex {
//     fn eval(self) -> Regex {
//         Regex::new(&self.text).unwrap()
//     }
// }
//
// pub struct TokenString {
//     text: String,
// }
//
// impl AST<String> for TokenString {
//     fn eval(self) -> String {
//         self.text
//     }
// }
