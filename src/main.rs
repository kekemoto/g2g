/*

RuleはRustでの入力規則と出力規則のデータ表現。

RuleをG2Gファイルから生成
    規則はRBNFで書かれ、RBNFをパースしてRuleを生成。

生成されたRuleを元に対象ファイルをパースし、コンバータ（出力用の構造体）を作成します。
    パーサーは対象ファイルを読み込み、Ruleが適用されるかを順番に確認していきます（PEGみたいな感じで確認）
        1文字ずつ読み込み、トークンに詰めていく。
        読み込みの度にトークンとルール要素を比較していく。
        字句の区切りのためにルール要素は先読みも行う。
    当てはまった場合は、Ruleを適用し、コンバータを作成
        当てはまった場合は、Ruleの出力規則からコンバータを作成。
        コンバータの＄変数配列に文字列を格納。格納データは正規表現で全て表されるんだろうなぁきっと。

コンバータから、文字列を出力します
    Ruleの出力規則にある＄部分を、コンバータの＄変数配列の中身と置換して出力する

メモ：
    コンバータを挟まなくてもパースした瞬間に文字列に置換していっても良いかも？
    RBNFのパーサーと、対象ファイルのパーサーの二つが必要。
    RBNFパーサーはいらないかも。RBNFのRuleを手書きすれば……

///// g2g file

array < '[' data (',' data)* ']'
array > [$1$2(, $1)*]

data < ( num | string )
data > $1($1|$1)

num < /\d+/
num > $1

string < '\'' /.* / '\''
string > "$1"

///// input file

[123, "hello"]

///// output file

[123, "hello"]

*/

#[macro_use]
extern crate lazy_static;
extern crate regex;

mod lexer;
mod rule;

use std::env;

fn main() {
    /*
        let rules = GGParser::run(gg_file);
        RuleParseer::run(target_file, rules);
    */

    let args: Vec<String> = env::args().collect();
    let file_path = &args[1];

    let tokens = lexer::lex(file_path);

    println!("{:?}", tokens);
}
